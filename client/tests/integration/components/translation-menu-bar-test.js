import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('translation-menu-bar', 'Integration | Component | translation menu bar', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{translation-menu-bar}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#translation-menu-bar}}
      template block text
    {{/translation-menu-bar}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
