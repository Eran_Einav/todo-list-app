import Service from '@ember/service';
import { inject as service } from '@ember/service';
import ENV from "client/config/environment";

export default Service.extend({
    
    intl: service(),
    initLocale(){

        var self = this;
        self.getLocale(function(locale){
            self.get('intl').setLocale(locale);
        });

    },
    setLocale(locale) {

        var ls = localStorage,
        tla = ls.getItem('tla'),
        tla_obj = {},
        tla_str = '';

        if (tla !== null){
            tla_obj = JSON.parse(tla);
            tla_obj.locale = locale;
        }
        else {
            tla_obj = {};
            tla_obj.locale = locale;
        }

        tla_str = JSON.stringify(tla_obj);
        ls.setItem('tla', tla_str);

    },
    getLocale(cb) {

        let defaultLocale = ENV.LOCALE,
        tla = localStorage.getItem('tla'),
        locale = (tla !== null && typeof JSON.parse(tla).locale !== 'undefined') ? JSON.parse(tla).locale : defaultLocale
        return cb(locale);

    }

});
