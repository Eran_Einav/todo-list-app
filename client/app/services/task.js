import Service from '@ember/service';

export default Service.extend({
    
    store: Ember.inject.service(),
    addTask(newTaskText = 'New task') {

        let task = {
            id: null,
            title: newTaskText,
            isDone: false,
            isTitleEdit: true,
            isNewTask: true
        };

        this.get('store').createRecord('task', task);

    },
    saveTask(task){

        task.set('isNewTask', false);
        task.set('isTitleEdit', false);
        task.save().then(function(data) {

        }, function(err) {

          // Error callback
          console.error('err', err);
          
        });

    },
    updateTask(task){

        this.get('store').findRecord('task', task.get('id')).then(function(task) {

            task.set('isTitleEdit', false);

            task.save().then(function(data) {

            }, function(err) {

              // Error callback
              console.error('err', err);

            });

        }, function(err) {
            
          // Error callback
          console.error('err', err);

        });

    },
    checkedTask(task){

        this.get('store').findRecord('task', task.get('id')).then(function(task) {

            task.set('isDone', !task.get('isDone'));

            task.save().then(function(data) {

            }, function(err) {

              // Error callback
              console.error('err', err);

            });

        }, function(err) {

          // Error callback
          console.error('err', err);

        });

    },
    editTaskTitle(task){

        task.set('isTitleEdit', true);

    },
    exitEditTitleMode(task){


        if (task.get('id') === null || task.get('id') == 0) return this.saveTask(task);

        this.updateTask(task);

    },
    keyUp(obj) {
        
        obj.$('.tla-task-title-form').blur();

    },
    focusTaskTitle(obj){

        obj.$('.tla-task-title-form').focus();

    },
    deleteTask(task) {

        this.get('store').findRecord('task', task.get('id'), { backgroundReload: false }).then(function(task) {

            task.destroyRecord();

        });

    },
    updateStatusBar(obj){

        let $wrapper = obj.$().parents('.ember-application').find('.tla-task-status-wrapper'),
        toFinishLen = obj.$('.tla-task:not(.tla-task-done)').length,
        donelLen = obj.$('.tla-task.tla-task-done').length,
        totalLen = obj.$('.tla-task').length;
        $wrapper.find('.tla-task-status-to-finish .tla-task-status-count').text(toFinishLen);
        $wrapper.find('.tla-task-status-done .tla-task-status-count').text(donelLen);
        $wrapper.find('.tla-task-status-total .tla-task-status-count').text(totalLen);
        
    }

});
