import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default Route.extend({
    
    locale: service(),
    beforeModel() {

        this.get('locale').initLocale();

    },
    model() {

        return this.store.findAll('task');

    }

});
