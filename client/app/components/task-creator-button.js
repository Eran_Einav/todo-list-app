import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({

    taskService: service('task'),
    actions: {

        addTask(newTaskText){

            this.get('taskService').addTask(newTaskText);

        }

    }

});
