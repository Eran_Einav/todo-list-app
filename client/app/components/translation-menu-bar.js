import config from '../config/environment';
import Component from '@ember/component';
import { computed, get } from '@ember/object';
import { inject as service } from '@ember/service';
import { lookupByFactoryType } from 'ember-intl/hydrate';

const { modulePrefix } = config;

export default Component.extend({

    isTranslationMenu: config.IS_TRANSLATION_MENU,
    intl: service(),
    locale: service(),
    activeLocale: computed.readOnly('intl.locale'),
    locales: computed(function() {

        return lookupByFactoryType('translations', modulePrefix).map(moduleName => moduleName.split('/').pop());

    }).readOnly(),

    selections: computed('locales.[]', 'activeLocale', function() {
    
        let active = get(this, 'activeLocale');

        return get(this, 'locales').map(locale => {

            return {

                locale: locale,
                active: active.indexOf(locale) > -1

            };

        });
        
    }).readOnly(),
    actions: {

        changeLocale(locale) {

            this.get('locale').setLocale(locale);

            return get(this, 'intl').set('locale', locale);

        }

    }

});
