import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({

    taskService: service('task'),
    actions: {

        editTaskTitle(task){

            this.get('taskService').editTaskTitle(task);

        },
        exitEditTitleMode(task){

            let title = task.get('title').replace(/(<([^>]+)>)/ig,"");

            task.set('title', title);

            if ( !title ) return task.set('fieldErrorText', 'errorMessageEmptyField');

            task.set('fieldErrorText', '');
            this.get('taskService').exitEditTitleMode(task);

        },
        keyUp(event) {
            
            if (event.keyCode === 13) this.get('taskService').keyUp(this); 

        }

    }

});