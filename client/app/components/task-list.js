import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({

    taskService: service('task'),
    didRender(){

        this._super(...arguments);
        this.get('taskService').focusTaskTitle(this);
        this.get('taskService').updateStatusBar(this);
        
    }

});
