import DS from 'ember-data';

export default DS.Model.extend({
    _id: DS.attr('number'),
    title: DS.attr('string'),
    isDone: DS.attr('boolean', { defaultValue: false }),
    isTitleEdit: DS.attr('boolean', { defaultValue: false }),
    isNewTask: DS.attr('boolean', { defaultValue: false })
});
