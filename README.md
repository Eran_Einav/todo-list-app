# Todo List App

Todo list application based on Ember.js & Laravel

## Getting Started

Open a terminal and navigate to where you want to clone the repository to, then run the following commands:

```
git clone https://Eran_Einav@bitbucket.org/Eran_Einav/todo-list-app.git
cd todo-list-app
```
The repository will be cloned into a new directory with name of "todo-list-app".
Then we are using `cd` command for changing the current working directory into the new "todo-list-app" directory, for the next steps.

### Prerequisites

1. [PHP](http://php.net/manual/en/install.php) and [MySQL](https://dev.mysql.com/doc/mysql-installation-excerpt/5.7/en). as an alternative, you can install [XAMPP](https://www.apachefriends.org), for every operating system.
2. [Composer](https://getcomposer.org).
3. [Node.js and npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm).

### Installing

In the terminal, Assuming you are already in the root of the project, run the following command:

```
npm run init-server-client:install
```
It should:

1. Install all the dependencies of the project.
2. Create .env file in the client directory with the content of .env.example file.
3. Create .env file in the server directory with the content of .env.example file and then generate a APP_KEY for the Laravel app.

We need to create database for the project and migrate the database tables:

1. Edit server/.env file and modify the values of DB_* keys according to your database credentials.
2. Make sure MySQL is running.
3. Run the following command:

```
npm run db:migrate
```
It should create the database and migrate the database tables.

## Running the servers

For starting the servers and running the project, run in the terminal the following command:

```
npm start
```
It should:

1. Run Laravel's server for the API interaction.
2. Run development server for Ember.
3. Open a new Chrome browser tab with the Ember app, after couple of seconds.

`npm start` is running `npm run all:start:chrome` by default. As an alternative, you can run the app in Firefox by running `npm run all:start:firefox` in the terminal or `npm run all:start` if you would like to open a different browser manually; the address is [http://localhost:4200](http://localhost:4200) by default.

## Options

You can change some default options of the Ember app via the client/.env file:

1. `API_HOST_URL`: the API base URL to work with (default: [`http://localhost:8000/api`](http://localhost:8000/api) - Laravel default server address with the api route)
2. `IS_TRANSLATION_MENU`: boolean option, if to display translation menu at the top of the app (default: `1`)
3. `LOCALE`: default locale for the Ember app, the hard coded texts will be translate by the locale. currently the options are `en-us` and `he-il` (default: `en-us`)

## Authors

*Eran Einav*

## License

This project is licensed under the [MIT](https://choosealicense.com/licenses/mit/) License