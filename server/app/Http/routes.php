<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['cors'], 'prefix' => 'api/tasks'], function(){
    Route::get('/', 'TaskController@getTasks');
    Route::get('/{id}', 'TaskController@getTask');
    Route::post('/', 'TaskController@createTask');
    Route::put('/{id}', 'TaskController@updateTask');
    Route::delete('/{id}', 'TaskController@deleteTask');
});

Route::get('/', function () {
    return redirect('api/tasks');
});

Route::get('/api', function () {
    return redirect('api/tasks');
});