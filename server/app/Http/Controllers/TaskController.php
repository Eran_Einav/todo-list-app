<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class TaskController extends Controller
{

    public function getTasks()
    {

        $tasks = [];
        $result = DB::table('tasks')->select('*')->get();

        foreach ($result as $key => $t) {
            $tasks[$key]['id'] = $t->task_id;
            $tasks[$key]['title'] = $t->task_title;
            $tasks[$key]['isDone'] = $t->task_isdone;
        }

        return response()->json(['tasks' => $tasks]);

    }

    public function getTask($task_id)
    {

        $result = DB::table('tasks')->where('task_id', $task_id)->select('*')->get();

        $task['id'] = $result[0]->task_id;
        $task['title'] = $result[0]->task_title;
        $task['isDone'] = $result[0]->task_isdone;

        return response()->json(['tasks' => $task]);

    }

    public function createTask(Request $request)
    {

        $this->validate($request, [
            'task.title' => array(
                'required',
                'regex:/^[A-Za-z1-9א-ת]/'
            )
        ]);

        $fields = request('task');
        $task_title = strip_tags($fields['title']);
        $task_isdone = $fields['isDone'];

        $task_id = DB::table('tasks')->insertGetId([
            'task_title' => $task_title,
            'task_isdone' => $task_isdone
        ]);

        $task['id'] = $task_id;

        return response()->json(['tasks' => $task]);

    }

    public function updateTask($task_id, Request $request)
    {

        $this->validate($request, [
            'task.title' => array(
                'required',
                'regex:/^[A-Za-z1-9א-ת]/'
            )
        ]);

        $fields = request('task');
        $task_title = strip_tags($fields['title']);
        $task_isdone = $fields['isDone'];

        $result = DB::table('tasks')->where('task_id', $task_id)->update([
            'task_title' => $task_title,
            'task_isdone' => $task_isdone
        ]);

        $task['id'] = $task_id;

        return response()->json(['tasks' => $task]);

    }

    public function deleteTask($task_id)
    {

        $result = DB::table('tasks')->where('task_id', $task_id)->delete();

        return response()->json(['tasks' => []]);

    }

}
